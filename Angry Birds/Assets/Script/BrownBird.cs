﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrownBird : Birds
{
    public PointEffector2D Explosion;
    public CircleCollider2D ExpoColl;



    private void Update()
    {
        if(_state == BirdState.Idle)
        {
            Explosion.enabled = false;
            ExpoColl.enabled = false;
        }

        if(_state == BirdState.HitSomething)
        {
            ExpoColl.enabled = true;
            Explosion.enabled = true;
            StartCoroutine(ExpoTime());
        }
    }

    IEnumerator ExpoTime()
    {
        yield return new WaitForSeconds(0.1f);
        Explosion.enabled = false;
        ExpoColl.enabled = false;
        //RigidBody.velocity = Vector2.zero;
    }
}
